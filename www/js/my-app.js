Template7.global = {
    android: Framework7.prototype.device.android === true,
    ios    : Framework7.prototype.device.ios === true,
    appName: "Tradeline Maps",
    ok     : "OK",
    cancel : "Cancel",
    loading: "Loading..."
};

var myApp = new Framework7({
    init                 : false,
    precompileTemplates  : true,
    material             : Framework7.prototype.device.android,
    smartSelectOpenIn    : 'picker',
    template7Pages       : false,
    modalTitle           : Template7.global.appName,
    modalButtonOk        : Template7.global.ok,
    modalButtonCancel    : Template7.global.cancel,
    modalPreloaderTitle  : Template7.global.loading,
    pushState            : false, /* #! on browser */
    tapHold              : false,
    cache                : false,
    swipePanel           : 'left',
    fastClicks           : false,
    onAjaxStart          : function (xhr) {
    },
    onAjaxComplete       : function (xhr) {
    },
    template7Data        : {},
    onPageBeforeInit     : function (app, page) {
        console.log('page before init ' + page.name);
        if (Framework7.prototype.device.android)
            $$('.navbar-through').addClass('navbar-fixed').removeClass('navbar-through');
    },
    onPageInit           : function (app, page) {
        console.log('page init ' + page.name);
    },
    onPageReinit         : function (app, page) {
        console.log('page reinit ' + page.name);
    },
    onPageBeforeAnimation: function (app, page) {
        console.log('page before animation ' + page.name);
    },
    onPageAfterAnimation : function (app, page) {
        console.log('page after animation ' + page.name);
        myApp.sizeNavbars();
    },
    onPageBeforeRemove   : function (app, page) {
        console.log('page before remove ' + page.name);
    },
    onPageBack           : function (app, page) {
        console.log('page back ' + page.name);
    },
    onPageAfterBack      : function (app, page) {
        console.log('page after back ' + page.name);
    }
});

var $$ = Dom7;

if (Framework7.prototype.device.android) {
    $$('.view .navbar').prependTo('.view .page');
    $$('head').append(
        '<link rel="stylesheet" href="lib/framework7/css/framework7.material.min.css">'
        + '<link rel="stylesheet" href="lib/framework7/css/framework7.material.colors.min.css">'
        + '<link rel="stylesheet" href="css/my-app.material.css">'
    );
} else {
    $$('head').append(
        '<link rel="stylesheet" href="lib/framework7/css/framework7.ios.css">'
        + '<link rel="stylesheet" href="lib/framework7/css/framework7.ios.colors.min.css">'
        + '<link rel="stylesheet" href="css/my-app.ios.css">'
    );
}

var mainView = myApp.addView('.view-main', {
    dynamicNavbar: Framework7.prototype.device.ios,
    url          : "index.html"
});

myApp.data = {
    fullName   : undefined,
    loggedIn   : undefined,
    gMapsLoaded: undefined
};

function initGoogleMaps() {
    myApp.data.gMapsLoaded = true;
    console.log("Google Maps Loaded");
    $$('body').on('touchend', '.pac-container', function (e) {
        e.stopPropagation();
    });
    // mainView.refreshPage();
}

$$(document).on('deviceready', function () {
    console.log("Device is ready!");
    console.log("facebookConnectPlugin", facebookConnectPlugin);
    myApp.init();
    myApp.showIndicator();
    console.log("myApp initialized on " + myApp.device.os);
});

myApp.onPageInit('index', function (page) {
    var loginFirstButton = $$(page.container).find("a.login");
    var getDirectionsButton = $$(page.container).find("a.get-directions");
    var nameField = $$(page.container).find(".welcome .name");
    var travelModeSelect = $$(page.container).find("select[name=travel-mode]");
    var originByLocation = $$(page.container).find(".item-input.origin a.current-location");
    var destinationByLocation = $$(page.container).find(".item-input.destination a.current-location");
    var originTextfield = $$(page.container).find("input[name=origin]")[0];
    var autocompleteOrigin = new google.maps.places.Autocomplete(originTextfield);
    var destinationTextfield = $$(page.container).find("input[name=destination]")[0];
    var autocompleteDestination = new google.maps.places.Autocomplete(destinationTextfield);
    var originPlace, destinationPlace;
    google.maps.event.addListener(autocompleteOrigin, 'place_changed', function () {
        originPlace = autocompleteOrigin.getPlace();
    });
    google.maps.event.addListener(autocompleteDestination, 'place_changed', function () {
        destinationPlace = autocompleteDestination.getPlace();
    });
    $$(originByLocation).click(function () {
        myApp.updateCurrentPosition(false, function () {
            if (!myApp.currentLocation.place)
                return;
            originPlace = myApp.currentLocation.place;
            $$(originTextfield).val(myApp.currentLocation.place.formatted_address);
        });
    });
    $$(destinationByLocation).click(function () {
        myApp.updateCurrentPosition(false, function () {
            if (!myApp.currentLocation.place)
                return;
            destinationPlace = myApp.currentLocation.place;
            $$(destinationTextfield).val(myApp.currentLocation.place.formatted_address);
        });
    });
    $$(getDirectionsButton).click(function () {
        if (!originPlace || !originPlace.place_id) {
            myApp.alert("Please select an origin from the ones provided.", "Select Origin");
            return;
        }
        if (!destinationPlace || !destinationPlace.place_id) {
            myApp.alert("Please select a destination from the ones provided.", "Select Destination");
            return;
        }
        mainView.router.load({
            url  : 'directions.html',
            query: {origin: originPlace, destination: destinationPlace, travelMode: $$(travelModeSelect).val()}
        });
    });
    var checkLoggedIn = function (response) {
        if (response.status === 'connected') {
            myApp.data.loggedIn = true;
            console.log('Welcome!  Fetching your information.... ');
            facebookConnectPlugin.api('me', ["public_profile"], function (response) {
                myApp.data.fullName = response.name;
                console.log('Successful login for: ' + response.name);
                nameField.text(myApp.data.fullName);
                loginFirstButton.hide();
                getDirectionsButton.show();
            }, console.error);
        } else {
            myApp.data.loggedIn = false;
            console.warn("User is logged out", response);
            nameField.text("visitor");
            loginFirstButton.show();
            getDirectionsButton.hide();
        }
        myApp.hideIndicator();
    };
    $$(loginFirstButton).click(function () {
        facebookConnectPlugin.login(["public_profile"], checkLoggedIn, console.error);
    });
    facebookConnectPlugin.getLoginStatus(checkLoggedIn, console.error);
});

/**
 * Map object for page directions.html
 * @type google.maps.Map
 */
myApp.map = undefined;

myApp.onPageInit('directions', function (page) {
    var updateCurrentLocation = $$(page.navbarInnerContainer).find("a.update-current-location");
    if (updateCurrentLocation.length === 0) /* android fix */
        updateCurrentLocation = $$(".page[data-page=directions] .navbar a.update-current-location");
    myApp.map = new google.maps.Map($$(page.container).find("#map")[0], {
        zoom            : 7,
        center          : {lat: 37.990832, lng: 23.7033199},
        mapTypeId       : 'roadmap',
        disableDefaultUI: true
    });
    $$(updateCurrentLocation).click(function () {
        myApp.updateCurrentPosition(true);
    });
});

myApp.currentLocation = {
    pos       : undefined,
    latLng    : undefined,
    marker    : undefined,
    infoWindow: undefined,
    place     : undefined
};

/**
 * Update user's position on each call in case he is moving.
 * @param centerMap boolean whether to center the map to the current location or not
 * @param onComplete function to call on complete
 */
myApp.updateCurrentPosition = function (centerMap, onComplete) {
    if (!navigator.geolocation) {
        console.error("Browser doesn't support Geolocation.");
        myApp.alert("Device doesn't support Geolocation.");
        return;
    }
    if (!myApp.currentLocation.infoWindow) {
        myApp.currentLocation.infoWindow = new google.maps.InfoWindow;
    }
    var geocoder = new google.maps.Geocoder;
    navigator.geolocation.getCurrentPosition(function (position) {
            myApp.currentLocation.pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            if (centerMap && myApp.map) {
                myApp.map.setZoom(15);
                myApp.map.setCenter(myApp.currentLocation.pos);
                myApp.currentLocation.infoWindow.open(myApp.map);
            }
            myApp.currentLocation.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            myApp.currentLocation.infoWindow.setPosition(myApp.currentLocation.pos);
            if (myApp.currentLocation.marker) {
                myApp.currentLocation.marker.setMap(null);
                myApp.currentLocation.marker = null;
            }
            geocoder.geocode({'location': myApp.currentLocation.latLng}, function (results, status) {
                var title = "Your position";
                if (status === 'OK') {
                    if (results[0]) {
                        myApp.currentLocation.place = results[0];
                        title = "Your position: " + results[0].formatted_address;
                    } else {
                        myApp.currentLocation.place = undefined;
                        console.error(status, results);
                    }
                    if (typeof onComplete === "function")
                        onComplete();
                } else {
                    myApp.currentLocation.place = undefined;
                    console.error('Geocoder failed due to: ' + status, results);
                }
                if (myApp.map) {
                    myApp.currentLocation.marker = new google.maps.Marker({
                        position : myApp.currentLocation.latLng,
                        map      : myApp.map,
                        title    : title,
                        animation: google.maps.Animation.DROP
                    });
                    myApp.currentLocation.infoWindow.setContent(title);
                }
            });
        }, function (error) {
            myApp.currentLocation.pos = undefined;
            myApp.currentLocation.latLng = undefined;
            if (myApp.currentLocation.marker)
                myApp.currentLocation.marker.setMap(null);
            myApp.currentLocation.marker = null;
            if (myApp.currentLocation.infoWindow)
                myApp.currentLocation.infoWindow.close();
            myApp.currentLocation.infoWindow = null;
            myApp.currentLocation.place = null;
            console.error(error);
            myApp.alert("Please check location services are enabled and this app has been given permissions.", "Error Retrieving Current Position");
        }, {enableHighAccuracy: true}
    );

};

myApp.onPageBeforeAnimation("directions", function (page) {
    var origin = typeof page.query.origin === "object" ? (page.query.origin.geometry.location || page.query.origin.name) : page.query.origin;
    var destination = typeof page.query.destination === "object" ? (page.query.destination.geometry.location || page.query.destination.name) : page.query.destination;
    var travelModes = "DRIVING,BICYCLING,TRANSIT,WALKING".split(',');
    var travelMode = travelModes.indexOf(page.query.travelMode) === -1 ? 'DRIVING' : page.query.travelMode;
    if (!origin) {
        console.error(origin, page.query.origin);
        mainView.router.back();
        myApp.alert("Something went wrong with the origin. Please try something else.", "Wrong Format");
        return;
    }
    if (!destination) {
        console.error(destination, page.query.destination);
        mainView.router.back();
        myApp.alert("Something went wrong with the destination. Please try something else.", "Wrong Format");
        return;
    }
    var distanceField = $$(page.container).find(".stats .distance");
    var durationField = $$(page.container).find(".stats .duration");
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(myApp.map);
    directionsService.route({
        origin     : origin,
        destination: destination,
        travelMode : travelMode,
        unitSystem : google.maps.UnitSystem.METRIC
    }, function (response, status) {
        switch (status) {
            case "OK":
                myApp.updateCurrentPosition();
                directionsDisplay.setDirections(response);
                distanceField.text(response.routes[0].legs[0].distance.text);
                durationField.text(response.routes[0].legs[0].duration.text);
                break;
            case "NOT_FOUND":
                mainView.router.back();
                myApp.alert("One of the places was not found, please try another one.", "Unable Finding Place");
                break;
            case "ZERO_RESULTS":
                mainView.router.back();
                myApp.alert("No directions could be found from the origin to the destination.\nPlease try something else.", "Unable Finding Directions");
                break;
            default:
                console.error('Directions request failed: ' + status, response);
                mainView.router.back();
                myApp.alert("An unexpected error occurred, please retry.", "Error Retrieving Directions");
                break;
        }
    });
});

myApp.onPageBeforeRemove("directions", function (page) {
    myApp.map = null;
});