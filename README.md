# Tradeline Maps

Login through Facebook and get directions to the place you want to go.

## Getting Started

```
git clone https://theofanis_@bitbucket.org/theofanis_/tradeline-maps.git
cd tradeline-maps
cordova prepare
cordova build
cordova-icon && cordova-splash # optional
```

An .apk for Android and an .ipa for iPhone should be available on [Diawi](https://www.diawi.com) for ad-hoc installations.

More at *https://bitbucket.org/theofanis_/tradeline-maps*

### Prerequisites

 1. **Node.js** -> Download from http://nodejs.org
 2. **Cordova** -> `$ npm install -g cordova` or PhoneGap -> `$ npm install -g phonegap`
 
 *Optional*
 
 3. cordova-icon -> `$ npm install -g cordova-icon`
 4. cordova-splash -> `$ npm install -g cordova-splash`
 
##### Additionally

 - for [**iOS**](https://cordova.apache.org/docs/en/latest/guide/platforms/ios/#installing-the-requirements)
 - for [**Android**](https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html#installing-the-requirements)
 
**Verify `$ cordova requirements`**
 
### Deployment

**For iOS**

 - [Simulator](https://cordova.apache.org/docs/en/latest/guide/platforms/ios/#deploying-to-simulator) `$ cordova emulate ios `
 - [Device](https://cordova.apache.org/docs/en/latest/guide/platforms/ios/#deploying-to-device) `$ cordova run ios --device`
  
**For [Android](https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html#requirements-and-support)**
 
 - `$ cordova run android --emulator`
 - `$ cordova run android --device`
  
## Built With

* [Cordova](https://cordova.apache.org) - Build an app for multiple platforms with one code base.
* [Framework7](https://framework7.io) - Full Featured HTML Framework For Building iOS & Android Apps

## Authors

* [**Theofanis Vardatsikos**](https://www.linkedin.com/in/theofanis-vardatsikos-27702090/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

